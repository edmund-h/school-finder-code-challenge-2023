//
//  SATService.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/29/23.
//

import Foundation


final class SATService {
    
    private static let schoolSATsURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4"
    
    static func getSATs(for school: School, completion: @escaping ()->()) {
        
        guard var urlComponents = URLComponents(string: schoolSATsURL) else { completion(); return }
        
        urlComponents.queryItems = [
            URLQueryItem(name: SATScore.CodingKeys.dbn.rawValue, value: school.schoolInfo.dbn)
        ]
        
        guard let url = urlComponents.url else{ completion(); return }
        
        let requestedItem = URLRequestableItem(url: url, requestedItem: [SATScore].self)
        
        URLService.get(item: requestedItem, completion: { item in
            guard let schoolSATData = item as? [SATScore],
                  let schoolSATScores = schoolSATData.first else { completion (); return }
            
            school.schoolSATScores = schoolSATScores
            completion()
        })
    }
    
}
