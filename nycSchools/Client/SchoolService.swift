//
//  SchoolService.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/29/23.
//

import Foundation


final class SchoolService {
    
    private static let schoolsURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2"
    
    static func getSchools(then completion: @escaping ([School])->()) {
        guard let url = URL(string: schoolsURL) else { completion([]); return }
        
        let urlItem = URLRequestableItem(url: url, requestedItem: [SchoolInfo].self)
        
        URLService.get(item: urlItem, completion: { item in
            guard let schoolInfo = item as? [SchoolInfo] else { completion([]); return }
            
            let schools = schoolInfo.map({ School(schoolInfo: $0) })
            completion(schools)
        })
    }
}
