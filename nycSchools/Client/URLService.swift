//
//  URLService.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/30/23.
//

import Foundation


struct URLRequestableItem {
    let url: URL
    let requestedItem: Decodable.Type
}


final class URLService {
    
    static func get(item: URLRequestableItem, completion: @escaping (Decodable?)->()) {
        
        let urlRequest = URLRequest(url: item.url)
        let task = URLSession(configuration: .default).dataTask(with: urlRequest, completionHandler: { data, response, error in
            guard let data = data else { completion(nil); return }
            let decoder = JSONDecoder()
            do {
                let decodedItem = try decoder.decode(item.requestedItem.self, from: data)
                completion(decodedItem)
                
            } catch {
                let dataJSON = try? decoder.decode(String.self, from: data)
                print("did not get item. instead got \( dataJSON ?? "nothing" )")
                completion(nil)
                return
            }
        })
        
        task.resume()
    }
    
}
