//
//  DetailViewController.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/30/23.
//

import UIKit
import WebKit


class DetailViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var schoolInfoBackgroundView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var studentsLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var school: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.allowsLinkPreview = true
        webView.navigationDelegate = self
        
        guard let school = school else { return }
        
        nameLabel.text = school.schoolInfo.name
        studentsLabel.text = school.schoolInfo.students ?? "###"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let school = school else { return }
        colorBackgroundView(for: school)
        
        if let scores = school.schoolSATScores {
            updateSATScores(with: scores)
        } else {
            SATService.getSATs(for: school, completion: {
                
                guard let scores = school.schoolSATScores else { return }
                DispatchQueue.main.async {
                    
                    self.navigateWebView()
                    self.updateSATScores(with: scores)
                }
            })
        }
    }
    
    func colorBackgroundView(for school: School) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = schoolInfoBackgroundView.bounds
        gradientLayer.colors = [school.schoolInfo.borough.color.cgColor, UIColor.white.cgColor]
        schoolInfoBackgroundView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func updateSATScores(with schoolSATScores: SATScore) {
        mathLabel.text = schoolSATScores.avgMath
        readingLabel.text = schoolSATScores.avgCritReading
        writingLabel.text = schoolSATScores.avgWriting
    }
    
    func navigateWebView() {
        guard let website = school?.schoolInfo.website, let url = URL(string: "https://" + website) else { return }
        
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
}
