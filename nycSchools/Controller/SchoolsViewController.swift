//
//  ViewController.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/29/23.
//

import UIKit


class SchoolsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView?
    
    var schools = [School]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "NYC Schools"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SchoolService.getSchools(then: { schools in
            self.schools = schools.sorted(by: { return $0.schoolInfo.name < $1.schoolInfo.name })
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSchoolDetail",
            let school = sender as? School,
            let destination = segue.destination as? DetailViewController {
            
            destination.school = school
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let schoolInfo = schools[indexPath.row].schoolInfo
        var config = UIListContentConfiguration.subtitleCell()
        config.text = schoolInfo.name
        config.secondaryText = schoolInfo.borough.rawValue
        cell.contentConfiguration = config
        cell.contentView.backgroundColor = schoolInfo.borough.color
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showSchoolDetail", sender: schools[indexPath.row])
    }
    
}

