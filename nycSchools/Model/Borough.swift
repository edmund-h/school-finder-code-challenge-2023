//
//  Borough.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/30/23.
//

import Foundation
import UIKit.UIColor


enum Borough: String, Codable {
    
    case Brooklyn, Bronx, Manhattan, Queens, StatenIsland = "Staten Island", Other
    
    var color: UIColor { return UIColor(named: self.rawValue)! }
    
    static func getFrom(string letter: String)-> Borough {
        switch letter{
            case "R": return .StatenIsland
            case "M": return .Manhattan
            case "K": return .Brooklyn
            case "Q": return .Queens
            case "X": return .Bronx
            default: return .Other
        }
    }
    
}
