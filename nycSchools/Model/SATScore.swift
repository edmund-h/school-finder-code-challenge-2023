//
//  SATScore.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/29/23.
//

import Foundation


struct SATScore: Codable {
    
    let dbn: String
    let avgCritReading: String
    let avgMath: String
    let avgWriting: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case avgCritReading = "sat_critical_reading_avg_score"
        case avgMath = "sat_math_avg_score"
        case avgWriting = "sat_writing_avg_score"
    }
    
}
