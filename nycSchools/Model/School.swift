//
//  School.swift
//  nycSchools
//
//  Created by Edmund Holderbaum on 3/29/23.
//

import Foundation


class School {
    let schoolInfo: SchoolInfo
    var schoolSATScores: SATScore?
    
    init(schoolInfo: SchoolInfo) {
        self.schoolInfo = schoolInfo
    }
}

struct SchoolInfo: Codable {
    
    let name: String
    let dbn: String
    let students: String?
    let website: String?
    
    private let boro: String?
    var borough: Borough { return Borough.getFrom(string: boro ?? "other")}
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case boro
        case website
        case name = "school_name"
        case students = "total_students"
    }
    
}
